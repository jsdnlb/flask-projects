# Esqueleto Flask

Estructura básica de carpetas en Flask

**1 -** Clonar repositorio.

```sh
    git clone https://github.com/danielbalzate/flask-projects.git flask-projects
    cd  flask-projects/my-hanged-rest
```

**2 -** Cree un entorno virtual de Python e instale Flask:

```sh
    virtualenv venv -p python3
    venv/bin/pip install flask
```

**3 -** Ejecute el proyecto de Flask con: (run.py debe tener permisos de ejecución: _chmod + x run.py_):

```sh
    ./run.py
```

Por defecto, este proyecto abrirá el puerto 5555, intente en su navegador con **localhost:5555**
